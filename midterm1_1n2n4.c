#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define CHECK(PRED) printf("%s ... %s\n", (PRED)? "passed" : "FAILED", #PRED)
#define BUFSIZE 1024

/* 1. */
size_t find_first(const char s[], int c);
void replace_first(int a[], size_t n, int oldint, int newint);
void replace_last(char s[], int oldc, int newc);
size_t count(const int a[], size_t n, int x);

/* 2. */
size_t count_alpha(const char s[]);
int all_digits(const char s[]);
void lowercase_copy(char dest[], const char src[]);
void reverse_copy(char dest[], const char src[]);
void ltrim_copy(char dest[], const char src[]);
int is_valid_id(const char s[]);

/* extra */
void reverse_copy_recursive(char dest[], const char src[]);
int reverse_recursive(char dest[], const char src[], size_t i);

int main(void)
{
    char strTemp[] = "Cheers! SEt 2D!";
    int iArray[] = {5, 4, 1, 1, -1, 0, -1, -2};
    char strDigits[] = "123456789o";
    char strID[] = "a12345678";
    char dest[BUFSIZE];

    char a[] = "hello";
    char *p = "world";

    CHECK(find_first(strTemp, 'e') == 2);

    replace_first(iArray, 8, -1, 1111);
    CHECK(iArray[4] == 1111);

    replace_last(strTemp, 'E', 'e');
    CHECK(strcmp(strTemp, "Cheers! Set 2D!") == 0);

    CHECK(count(iArray, 8, 1) == 2);

    CHECK(count_alpha(strTemp) == 10);

    CHECK(all_digits(strDigits) == 0);

    lowercase_copy(dest, strTemp);
    CHECK(strcmp(dest, "cheers! set 2d!") == 0);

    reverse_copy(dest, strDigits);
    CHECK(strcmp(dest, "o987654321") == 0);

    ltrim_copy(dest, strTemp);
    CHECK(strcmp(dest, "Cheers!Set2D!") == 0);

    CHECK(is_valid_id(strID) == 1);

    reverse_copy_recursive(dest, strDigits);
    CHECK(strcmp(dest, "o987654321") == 0);

    /* 4. */
    p = &a[1];
    /*p[1] = *a;*/ /* invalid */
    /*a = p;*/ /* invalid */
    printf("%s", p);
    printf("\n");
    
    return 0;
}

size_t find_first(const char s[], int c)
{
    size_t i;
    for (i = 0; s[i] != '\0'; i++)
    {
        if (s[i] == c)
        {
            return i;
        }
    }

    return (size_t)-1;
}

void replace_first(int a[], size_t n, int oldint, int newint)
{
    size_t i;
    for (i = 0; i < n; i++)
    {
        if (a[i] == oldint)
        {
            a[i] = newint;
            return;
        }
    }
}

void replace_last(char s[], int oldc, int newc)
{
    size_t len = strlen(s); /* needs 'string.h' */
    size_t i;
    for (i = len; i > 0; i--)
    {
        if (s[i - 1] == oldc)
        {
            s[i - 1] = newc;
            return;
        }
    }
}

size_t count(const int a[], size_t n, int x)
{
    size_t count = 0;
    size_t i;
    for (i = 0; i < n; i++)
    {
        if (a[i] == x)
        {
            count++;
        }
    }

    return count;
}

size_t count_alpha(const char s[])
{
    size_t count = 0;
    size_t i;
    for (i = 0; s[i] != '\0'; i++)
    {
        if (isalpha(s[i])) /* needs 'ctype.h' */
        {
            count++;
        }
    }

    return count;
}

int all_digits(const char s[])
{
    size_t i;
    for (i = 0; s[i] != '\0'; i++)
    {
        if (!isdigit(s[i])) /* needs 'ctype.h' */
        {
            return 0;
        }
    }

    return 1;
}

void lowercase_copy(char dest[], const char src[])
{
    size_t i;
    for (i = 0; src[i] != '\0'; i++)
    {
        dest[i] = tolower(src[i]); /* needs 'ctype.h' */
    }

    dest[i] = '\0';
}

void reverse_copy(char dest[], const char src[])
{
    size_t len = strlen(src); /* needs 'string.h' */
    size_t i;
    for (i = len; i > 0; i--)
    {
        dest[len - i] = src[i - 1];
    }

    dest[len] = '\0';
}

void ltrim_copy(char dest[], const char src[])
{
    size_t index = 0;
    size_t i;
    for (i = 0; src[i] != '\0'; i++)
    {
        if (!isspace(src[i]))
        {
            dest[index++] = src[i];
        }
    }

    dest[index] = '\0';
}

int is_valid_id(const char s[])
{
    size_t len = strlen(s); /* needs 'string.h' */
    size_t i;

    if (len != 9)
    {
        return 0;
    }

    if (toupper(s[0]) != 'A') /* needs 'ctype.h' */
    {
        return 0;
    }

    for (i = 1; i < len; i++)
    {
        if (!isdigit(s[i])) /* needs 'ctype.h' */
        {
            return 0;
        }
    }

    return 1;
}

void reverse_copy_recursive(char dest[], const char src[])
{
    reverse_recursive(dest, src, 0);
}

int reverse_recursive(char dest[], const char src[], size_t i)
{
    if (src[i] == '\0') {
        dest[i] = '\0';
        return 0;
    } else {
        int index = reverse_recursive(dest, src, i + 1);
        dest[index] = src[i];
        return index + 1;
    }
}
luxes
