#include <stdio.h>
#include <ctype.h>

#define CHECK(PRED) printf("%s ... %s\n", (PRED)? "passed" : "FAILED", #PRED)
#define BUFSIZE 1024

size_t count_alpha(const char s[]);

int main(int argc, char *argv[])
{
    FILE *fp;
    char strBuf[BUFSIZE];
    size_t countAlpha = 0;

    if (argc != 2)
        return 0;

    if (!(fp = fopen(argv[1], "rb")))
    {
        perror("fopen");
        return 0;
    }

    while (fgets(strBuf, BUFSIZE, fp))
    {
        printf("%s", strBuf);
        countAlpha += count_alpha(strBuf);
    }

    CHECK(countAlpha == 3);

    fclose(fp);
    
    return 0;
}

size_t count_alpha(const char s[])
{
    size_t count = 0;
    size_t i;
    for (i = 0; s[i] != '\0'; i++)
    {
        if (isalpha(s[i])) /* needs 'ctype.h' */
        {
            count++;
        }
    }

    return count;
}
luxes
