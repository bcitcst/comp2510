#include <stdio.h>

int main(void)
{
    int i, j;
    int n;
    int a, b, c;
    int m, l;
    int *p, *q, *r;
    int **pp, **qq;
    char aj[32] = "0123456789";
    float f;
    unsigned short sm, sn;

    for (i = 2; i < 10; i++) {
        putchar('*');
        if (i > 7)
            break;
    }
    printf("\n");

    for (i = 1; i < 10; i++) {
        if (i % 3 != 1)
            continue;
        putchar('*');
    }
    printf("\n");

    for (i = 1; i < 8; i += 2)
        for (j = 0; j <= i; j++)
            putchar('*');
    printf("\n");

    n = 7;
    while (n > 2)
        printf("%d ", --n);
    printf("\n");

    n = 7;
    while (n-- > 2)
        printf("%d ", n);
    printf("\n");

    n = 7;
    while (--n > 2)
        printf("%d ", n);
    printf("\n");

    a = 1; b = 1; c = -1;
    c = --a && b++;
    printf("%d %d %d", a, b, c);
    printf("\n");

    a = 0; b = 0; c = -1;
    c = a++ || ++b;
    printf("%d %d %d", a, b, c);
    printf("\n");

    m = 11; n = 22;
    p = &m;
    q = &n;
    q = p;
    m++; n--;
    printf("%d %d", *p, *q);
    printf("\n");

    m = 11; n = 22;
    p = &m;
    q = &n;
    *q = *p;
    m++; n--;
    printf("%d %d", *p, *q);
    printf("\n");

    m = 11; n = 22;
    p = &m;
    q = &n;
    *q = *p + 1;
    *p = *q + 1;
    printf("%d %d", m, n);
    printf("\n");

    m = 11; n = 22;
    p = &m;
    q = &n;
    pp = &p;
    qq = &q;
    r = *pp;
    *pp = *qq;
    *qq = r;
    printf("%d %d", *p, *q);
    printf("\n");

    sscanf("hi", "%s", &aj[4]);
    printf("%s", &aj[2]);
    printf("\n");

    m = 4; n = 5; l = 6;
    m = sscanf("    12hello 34", "%d %d", &n, &l);
    printf("%d %d %d", m, n, l);
    printf("\n");

    m = 4; n = 5; l = 6;
    m = sscanf("12-34.50", "%d %d", &n, &l);
    printf("%d %d %d", m, n, l);
    printf("\n");

    m = 4; n = 5;
    f = 1.0;
    m = sscanf("34.50", "%f %d", &f, &n);
    printf("%d %d %f", m, n, f);
    printf("\n");

    sm = 0x89ab; sn = 0xef67;

    printf("0x%x", sm & sn);
    printf("\n");

    printf("0x%x", ~sm | sn);
    printf("\n");

    printf("0x%x", sm ^ sn);
    printf("\n");

    return 0;
}
luxes
